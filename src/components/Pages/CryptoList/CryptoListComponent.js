import React, { Component } from 'react';
import styles from './CryptoList.module.scss';
import Select from '../../Select'
import Table from '../../Table'

import { MAX_CURRENCIES_TO_VIEW } from '../../../constants/Constants'


class cryptoList extends Component {
  constructor(props) {
  super(props);

  this.itemToAdd = this.itemToAdd.bind(this);
  this.removeItem = this.removeItem.bind(this);
  }

  componentDidMount() {
    this.props.getAllCoinNames()
    .then(() => this.props.updateList())
  }

  itemToAdd(id) {
    this.props.updateList('add', id);
  }

  removeItem(id) {
    this.props.updateList('remove', id);

    this.props.tableList.totalItems === 1 && this.props.updateList();
  }

  render (){
    const { tableList } = this.props
    const maxItemsReached = tableList.totalItems === MAX_CURRENCIES_TO_VIEW;

    const priceTableHead = [
      { rank: 'CMC Rank' },
      { symbol: 'Symbol' },
      { price: 'Price (in USD)' }
    ];

    const tableRowItems = Object.keys(tableList.list).map((key) => {
      return {
        rank: tableList.list[key].rank,
        symbol: tableList.list[key].symbol,
        price: `$ ${tableList.list[key].price}`,
        id: tableList.list[key].id
      }
    })
    return (
        <div className={styles.root}>
            <div className={styles.currencyWrapper}>
                <Select disabled={maxItemsReached} items={this.props.allCoins} itemSelected={(id) => this.itemToAdd(id)} />
            </div>
            <Table tableHead={priceTableHead} tableRowItems={tableRowItems} removeItem={(id) => this.removeItem(id)} />
        </div>
    );
  }
}

export default cryptoList;
