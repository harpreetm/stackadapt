
import { connect } from 'react-redux';
import CryptoListComponent from './CryptoListComponent';

import {
	getAllCoinNames,
	updateList,
} from '../../../actions';

const mapStateToProps = (state) => ({
	allCoins: state.allCoins,
	tableList: state.tableList
});

const mapDispatchToProps = (dispatch) => ({
	getAllCoinNames: () => dispatch(getAllCoinNames()),
	updateList: (condition, id) => dispatch(updateList(condition, id)),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(CryptoListComponent);