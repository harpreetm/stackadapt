
import * as React from 'react'
import styles from './select.module.scss'

function SelectComponent ({ items, itemSelected, disabled}) {
  return (
    <select className={styles.root} onChange={(e) => itemSelected(e.target.value)} disabled={disabled}>
      <option value="" defaultValue>Select a coin</option>
      {Object.keys(items.allCoins).map((item) => {
        const { id, name, symbol, addedToTable } = items.allCoins[item]

        return !addedToTable && (
          <option key={id} value={id}>{`${name} (${symbol})`}</option>
        )
      })}
    </select>
  )
}

export default SelectComponent;
