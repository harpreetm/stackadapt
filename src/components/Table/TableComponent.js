import * as React from 'react'
import styles from './Table.module.scss'

function TableComponent ({ tableHead, tableRowItems, removeItem }) {
  const tableColumnKeys = (tableHead || [])
    .map((item) => {
      return Object.keys(item)[0]
    })

  const tableColumnNames = (tableHead || [])
    .map((item) => {
      return Object.values(item)[0]
    })

  return (
    <table className={styles.root}>
      <TableHead items={tableColumnNames} />

      <tbody>
        {
          tableRowItems.map((item, index) => {
            const columnValues = tableColumnKeys
              .reduce((acc, tableColumnKey) => {
                const columnValue = item[tableColumnKey]
                acc.push(columnValue)

                return acc
              }, [])

            return <TableRow key={index} id={item.id} columnValues={columnValues} removeItem={removeItem} />
          })
        }
      </tbody>
    </table>
  )
}

function TableHead ({ items }) {
  return (
    <thead>
      <tr>
        {
          items.map((item, index) => {
            return <th key={index} className={styles.columnHead}>{item}</th>
          })
        }
        <th className={styles.columnHead}></th>
      </tr>
    </thead>
  )
}

function TableRow ({ id, columnValues, removeItem }) {
  return (
    <tr>
      {
        columnValues.map((item, index) => {
          return <td key={index} className={styles.columnBody}>{item}</td>
        })
      }
      <td onClick={() => removeItem(id)} className={styles.columnBody}><span className={styles.remove}>Remove</span></td>
    </tr>
  )
}

export default TableComponent
