import {
    ADD_TO_LIST,
    REMOVE_FROM_LIST,
    REMOVE_FROM_SEARCH_OPTIONS,
    ADD_TO_SEARCH_OPTIONS,
} from '../constants/ActionTypes';

import { GET_COIN_DETAILS } from '../constants/Constants'

export const updateList = (condition, id) => (dispatch, getState) => {
  const state = getState();
  let toAddIDs = '';

  if (condition === 'add') {
    toAddIDs = [id];
  } else if (condition === 'remove') {
    const list = state.tableList.list;
    delete list[id];

    dispatch({
      type: REMOVE_FROM_LIST,
      data: list
    })
    return dispatch({
      type: ADD_TO_SEARCH_OPTIONS,
      data: id
    })
  } else {
    toAddIDs = Object.keys(state.allCoins.allCoins).splice(0, 5)
  }

  dispatch({
    type: REMOVE_FROM_SEARCH_OPTIONS,
    data: toAddIDs
  })

  return fetch(`${GET_COIN_DETAILS}?id=${toAddIDs.join()}`)
  .then(response => response.json(), error => console.log('An error occurred.', error))
  .then(json => {

    const data = {}
    data['allCoins'] = state.allCoins.allCoins
    data['coinDetails'] = json.data
    dispatch({
      type: ADD_TO_LIST,
      data
    })
  });
};
