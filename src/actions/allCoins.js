import {
    ALL_COINS
} from '../constants/ActionTypes';

import {
	GET_ALL_COINS
} from '../constants/Constants';

export const getAllCoinNames = () => (dispatch) => {
  return fetch(GET_ALL_COINS)
  .then(response => response.json(), error => console.log('An error occurred.', error))
  .then(json => {
      dispatch({
        type: ALL_COINS,
        data: json.data
      })
    }
  )
};



