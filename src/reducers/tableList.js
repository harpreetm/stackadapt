import {
    ADD_TO_LIST,
    REMOVE_FROM_LIST
} from '../constants/ActionTypes';

const defaultState = {
    list: {},
    totalItems: ''
}

const updateListReducer = (coinsData, state) => {
  const { coinDetails, allCoins } = coinsData;
  const toAdd =  Object.keys(coinDetails).reduce((acc, coin) => {
    acc[coin] = {
      id: coinDetails[coin].id,
      symbol: coinDetails[coin].symbol,
      price: coinDetails[coin].quote.USD.price,
      rank: allCoins[coin].rank
    }
    return acc
  }, {})

  return {...state.list, ...toAdd};
}

const updateList = (state = defaultState, action) => {
	switch (action.type) {
    case ADD_TO_LIST:
      const list = updateListReducer(action.data, state)
      return {
        ...state,
        list,
        totalItems: Object.keys(list).length
      };
    case REMOVE_FROM_LIST:
      return {
        ...state,
        list: action.data,
        totalItems: Object.keys(action.data).length
      };
    default:
      return state;
  }
}
export default updateList;
