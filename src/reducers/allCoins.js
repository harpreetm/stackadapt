import {
    ALL_COINS,
    REMOVE_FROM_SEARCH_OPTIONS,
    ADD_TO_SEARCH_OPTIONS
} from '../constants/ActionTypes';

const defaultState = {
    allCoins: []
}

const allCoinsReducer = (data = []) => {
  const coins = data.map((coin) => {
      return {
        id: coin.id,
        name: coin.name,
        symbol: coin.symbol,
        rank: coin.rank,
        addedToTable: false
      }
    })

  return coins.reduce((coin, acc) => {
    return {
      ...coin,
      [acc['id']]: acc
    }
  }, {})
}

const removeFromSearchOptionsReducer = (allCoins, ids) => {
  ids.forEach(item => {
    allCoins[item].addedToTable = true;
  })
  return allCoins;
}

const addToSearchOptionsReducer = (allCoins, id) => {
  allCoins[id].addedToTable = false;
  return allCoins;
}
const allCoins = (state = defaultState, action) => {
	switch (action.type) {
    case ALL_COINS:
        return {
            ...state,
            allCoins: allCoinsReducer(action.data)
        };
    case REMOVE_FROM_SEARCH_OPTIONS:
        return {
            ...state,
            allCoins: removeFromSearchOptionsReducer(state.allCoins, action.data)
        };
    case ADD_TO_SEARCH_OPTIONS:
        return {
            ...state,
            allCoins: addToSearchOptionsReducer(state.allCoins, action.data)
        };
        default:
          return state;
    }
}
export default allCoins;
