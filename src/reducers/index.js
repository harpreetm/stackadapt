import { combineReducers } from 'redux';
import allCoins from './allCoins';
import tableList from './tableList';

const appReducer = combineReducers({
    allCoins,
    tableList
});

const index = (state, action) => appReducer(state, action);

export default index;
