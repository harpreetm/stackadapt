export const ALL_COINS = 'ALL_COINS';
export const ADD_TO_LIST = 'ADD_TO_LIST';
export const REMOVE_FROM_LIST = 'REMOVE_FROM_LIST';
export const REMOVE_FROM_SEARCH_OPTIONS = 'REMOVE_FROM_SEARCH_OPTIONS';
export const ADD_TO_SEARCH_OPTIONS = 'ADD_TO_SEARCH_OPTIONS';
