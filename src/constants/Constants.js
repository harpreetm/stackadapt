const API_ENDPOINT_ALL_COINS = 'https://www.stackadapt.com/coinmarketcap/map';
const API_ENDPOINT_COIN_DETAILS = 'https://www.stackadapt.com/coinmarketcap/quotes';

export const GET_ALL_COINS = API_ENDPOINT_ALL_COINS;
export const GET_COIN_DETAILS = API_ENDPOINT_COIN_DETAILS;
export const DEFAULT_CURRENCIES_TO_VIEW = 5;
export const MAX_CURRENCIES_TO_VIEW = 10;
